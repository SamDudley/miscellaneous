import requests


class Crud:  # (object):

    def __init__(self, url, username, password):
        self.url = url
        self.username = username
        self.password = password


    def create(self, payload):
        '''Document collection POST'''
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        c = requests.post(
            self.url + 'documents?form=testform&computewithform=true',
            headers=headers,
            auth=(self.username, self.password),
            json=payload
            )
        print(c.status_code)
        print(c.text)


    def read(self, view, params):
        '''View/folder entries GET'''
        r = requests.get(
            self.url + 'collections/name/{}'.format(view),
            auth=(self.username, self.password),
            params=params
            )
        print(r.status_code)
        return r.json()


    def update(self, unid, payload):
        '''Document PATCH'''
        headers = {
            'X-HTTP-Method-Override': 'PATCH',
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        u = requests.post(
            self.url + 'documents/unid/{}'.format(unid),
            headers=headers,
            auth=(self.username, self.password),
            json=payload
            )
        print(u.status_code)


    def delete(self, unid):
        '''Document DELETE'''
        d = requests.delete(
            self.url + 'documents/unid/{}'.format(unid),
            auth=(self.username, self.password)
            )
        print(d.status_code)

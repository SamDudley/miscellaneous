# README #

Small repo to host a few examples of my work.

### csv_compare.py
Compares 2 csv files, field by field.

Useful for confirming the changes made or side effects caused, in data
manipulation/cleaning/migration.

Compares line for line, so order or rows does matter.

Usage:
````
python csv_compare.py file_1.csv file_2.csv output.csv 
````

### dds_crud.py
Provides a way to easily interact with IBM's Domino Data Service (REST API).

dds_crud_test.py provides some examples of how this small library may be used.
import csv
import sys


import_file = sys.argv[1]
export_file = sys.argv[2]
report_file = sys.argv[3]


with open(import_file, 'r', newline='') as import_csv:
    imported_data = csv.DictReader(import_csv, delimiter=',')
    imported_data = list(imported_data)

    with open(export_file, 'r', newline='') as export_csv:
        exported_data = csv.DictReader(export_csv, delimiter=',')
        exported_data = list(exported_data)

        with open(report_file, 'w', newline='') as errors_csv:
            writer = csv.writer(errors_csv, delimiter=',')

            writer.writerow(['key', 'fieldname', 'imported_value', 'exported_value'])

            # counter
            errors = 0
            for i, row in enumerate(imported_data):
                for key, imported_value in dict.items(row):
                    exported_value = exported_data[i][key]
                    if imported_value == exported_value:
                        pass
                    else:
                        errors += 1
                        writer.writerow([row['key'], key, imported_value, exported_value])

print('differences found:', errors)

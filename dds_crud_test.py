import dds_crud
import pprint as pp


crud = dds.Crud('https://server/database/api/data/', 'username', 'password')


def test_create():
    firstname = input('firstname: ')
    lastname = input('lastname: ')
    age = input('age: ')
    payload = {
        'firstname': firstname,
        'lastname': lastname,
        'age': age
        # 'date': '2011-09-21T20:21:00Z',
        # 'array': ['text1', 'text2']
    }
    crud.create(payload)


def test_read():
    params = {
        'start': 0,
        'count': 5
        # 'sortcolumn': 'lastname',
        # 'sortorder': 'descending',
        # 'startkeys': 'foo'
    }
    result = crud.read('testview', params)
    pp.pprint(result)
    last_unid = result[-1]['@unid']
    return last_unid


def test_update(unid):
    age = int(input('age: '))
    payload = {
        'age': age
    }
    crud.update(unid, payload)


def test_delete(unid):
    crud.delete(unid)
